# ssh-client

Simple packaging for golang.org/x/crypto/ssh

### Install

```
go get gitee.com/zhangsq-cn/ssh-client
```

### Usage Example

```go
package main

import (
    "gitee.com/zhangsq-cn/ssh_client"
    "fmt"
)

func main()  {
    client, err := ssh_client.NewSSHClient("IP:port", "username", "password")
    if err != nil {
        panic(err)
    }

    username, err = client.Output("whoami")
    if err != nil {
        panic(err)
    }

    fmt.Println(string(username))
}
```

### Test

```
$ export TEST_HOST="ip:port"
$ export TEST_USER="username"
$ export TEST_PWD="password"
$ go test -v
```

