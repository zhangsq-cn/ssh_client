package ssh_client

import (
	"bytes"
	"fmt"
	"os"
	"testing"
	"time"
)

var client *SSHClient

func TestNewSSHClient(t *testing.T) {
	var err error
	client, err = NewSSHClient(os.Getenv("TEST_HOST"), os.Getenv("TEST_USER"), os.Getenv("TEST_PWD"))
	if err != nil {
		t.Error(err)
	}
}

func TestSSHClient_Run(t *testing.T) {
	err := client.Run("touch ./ssh-client-test-file.txt")
	if err != nil {
		t.Error(err)
	}

	err = client.Run("rm -rf ./ssh-client-test-file.txt")
	if err != nil {
		t.Error(err)
	}
}

func TestSSHClient_Output(t *testing.T) {
	output, err := client.Output("whoami")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(string(bytes.Trim(output, "\r\n ")), os.Getenv("TEST_USER"))
}

func TestSSHClient_Start(t *testing.T) {
	session, err := client.Start("ping google.com")
	if err != nil {
		t.Error(err)
	}
	time.Sleep(5 * time.Second)
	err = session.Close()
	if err != nil {
		t.Error(err)
	}
}

